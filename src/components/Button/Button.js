import React from "react";
import "./Button.scss";

function Button(props) {
  let a = 'abc';
  const { variant, children, ...rest } = props;
  return (
    <div className="btn-con">
      <button className={`button ${variant}`} {...rest}>
        {children}
      </button>
    </div>
  );
}

export default Button;
