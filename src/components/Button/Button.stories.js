import React from "react";
import Button from "./Button";

export default {
  title: "Button",
  component: Button,
};

export const Primary = () => <Button variant="primary">Main Button</Button>;
export const Outlined = () => <Button variant="outlined">Outlined</Button>;
export const Secondary = () => <Button variant="attachment"> + </Button>;
