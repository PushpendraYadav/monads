import React from "react";
import Badge from "react-bootstrap/Badge";
import Button from "react-bootstrap/Button";
import "bootstrap/dist/css/bootstrap.css";
import "./Badges.scss";

function Badges(props) {
  const { badgeType, pill, children, ...rest } = props;
  console.log(pill);
  return (
    <>
      {pill ? (
        <Badge pill className={`badge ${badgeType}`}>
          {children}
        </Badge>
      ) : (
        <Badge className={`badge ${badgeType}`}>{children}</Badge>
      )}
    </>
  );
}

export default Badges;
